import 'package:webtrit_phone/repositories/repositories.dart';

const dAccountInfo = AccountInfo(
  login: '1234567890',
  billingModel: BillingModel.debit,
  balance: 100,
  currency: 'USD',
  extensionName: '1',
  firstname: 'Agent',
  lastname: 'Smith',
  ext: '0000',
);
