import 'package:webtrit_phone/models/models.dart';

const dFavorites = [
  Favorite(
    id: 0,
    number: '1234',
    label: 'ext',
    contact: Contact(
      id: 0,
      sourceType: ContactSourceType.external,
      sourceId: '',
      displayName: 'Thomas Anderson',
    ),
  ),
  Favorite(
    id: 0,
    number: '2345',
    label: 'ext',
    contact: Contact(
      id: 0,
      sourceType: ContactSourceType.external,
      sourceId: '',
      displayName: 'Anna Collins',
    ),
  ),
  Favorite(
    id: 0,
    number: '3456',
    label: 'ext',
    contact: Contact(
      id: 0,
      sourceType: ContactSourceType.external,
      sourceId: '',
      displayName: 'Lawrence Brown',
    ),
  ),
  Favorite(
    id: 0,
    number: '4567',
    label: 'ext',
    contact: Contact(
      id: 0,
      sourceType: ContactSourceType.external,
      sourceId: '',
      displayName: 'Ruth Jenkins',
    ),
  ),
  Favorite(
    id: 0,
    number: '5678',
    label: 'ext',
    contact: Contact(
      id: 0,
      sourceType: ContactSourceType.external,
      sourceId: '',
      displayName: 'Beverly Nelson',
    ),
  ),
  Favorite(
    id: 0,
    number: '6789',
    label: 'ext',
    contact: Contact(
      id: 0,
      sourceType: ContactSourceType.external,
      sourceId: '',
      displayName: 'Randy Jones',
    ),
  ),
];
