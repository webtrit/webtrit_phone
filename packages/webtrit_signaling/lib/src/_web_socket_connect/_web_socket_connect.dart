Future<Stream<dynamic>> connect(
  String url, {
  Iterable<String>? protocols,
  Duration? connectionTimeout,
  Duration? pingInterval,
}) {
  throw UnsupportedError('No implementation of the api provided');
}
