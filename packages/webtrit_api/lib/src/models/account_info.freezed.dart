// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'account_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AccountInfo _$AccountInfoFromJson(Map<String, dynamic> json) {
  return _AccountInfo.fromJson(json);
}

/// @nodoc
mixin _$AccountInfo {
  String get login => throw _privateConstructorUsedError;
  BillingModel get billingModel => throw _privateConstructorUsedError;
  BalanceControlType? get balanceControlType =>
      throw _privateConstructorUsedError;
  double get balance => throw _privateConstructorUsedError;
  double? get creditLimit => throw _privateConstructorUsedError;
  String get currency => throw _privateConstructorUsedError;
  String? get extensionName => throw _privateConstructorUsedError;
  String? get firstname => throw _privateConstructorUsedError;
  String? get lastname => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get mobile => throw _privateConstructorUsedError;
  String? get companyName => throw _privateConstructorUsedError;
  String? get ext => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AccountInfoCopyWith<AccountInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AccountInfoCopyWith<$Res> {
  factory $AccountInfoCopyWith(
          AccountInfo value, $Res Function(AccountInfo) then) =
      _$AccountInfoCopyWithImpl<$Res, AccountInfo>;
  @useResult
  $Res call(
      {String login,
      BillingModel billingModel,
      BalanceControlType? balanceControlType,
      double balance,
      double? creditLimit,
      String currency,
      String? extensionName,
      String? firstname,
      String? lastname,
      String? email,
      String? mobile,
      String? companyName,
      String? ext});
}

/// @nodoc
class _$AccountInfoCopyWithImpl<$Res, $Val extends AccountInfo>
    implements $AccountInfoCopyWith<$Res> {
  _$AccountInfoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? login = null,
    Object? billingModel = null,
    Object? balanceControlType = freezed,
    Object? balance = null,
    Object? creditLimit = freezed,
    Object? currency = null,
    Object? extensionName = freezed,
    Object? firstname = freezed,
    Object? lastname = freezed,
    Object? email = freezed,
    Object? mobile = freezed,
    Object? companyName = freezed,
    Object? ext = freezed,
  }) {
    return _then(_value.copyWith(
      login: null == login
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as String,
      billingModel: null == billingModel
          ? _value.billingModel
          : billingModel // ignore: cast_nullable_to_non_nullable
              as BillingModel,
      balanceControlType: freezed == balanceControlType
          ? _value.balanceControlType
          : balanceControlType // ignore: cast_nullable_to_non_nullable
              as BalanceControlType?,
      balance: null == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as double,
      creditLimit: freezed == creditLimit
          ? _value.creditLimit
          : creditLimit // ignore: cast_nullable_to_non_nullable
              as double?,
      currency: null == currency
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      extensionName: freezed == extensionName
          ? _value.extensionName
          : extensionName // ignore: cast_nullable_to_non_nullable
              as String?,
      firstname: freezed == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String?,
      lastname: freezed == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      mobile: freezed == mobile
          ? _value.mobile
          : mobile // ignore: cast_nullable_to_non_nullable
              as String?,
      companyName: freezed == companyName
          ? _value.companyName
          : companyName // ignore: cast_nullable_to_non_nullable
              as String?,
      ext: freezed == ext
          ? _value.ext
          : ext // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AccountInfoCopyWith<$Res>
    implements $AccountInfoCopyWith<$Res> {
  factory _$$_AccountInfoCopyWith(
          _$_AccountInfo value, $Res Function(_$_AccountInfo) then) =
      __$$_AccountInfoCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String login,
      BillingModel billingModel,
      BalanceControlType? balanceControlType,
      double balance,
      double? creditLimit,
      String currency,
      String? extensionName,
      String? firstname,
      String? lastname,
      String? email,
      String? mobile,
      String? companyName,
      String? ext});
}

/// @nodoc
class __$$_AccountInfoCopyWithImpl<$Res>
    extends _$AccountInfoCopyWithImpl<$Res, _$_AccountInfo>
    implements _$$_AccountInfoCopyWith<$Res> {
  __$$_AccountInfoCopyWithImpl(
      _$_AccountInfo _value, $Res Function(_$_AccountInfo) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? login = null,
    Object? billingModel = null,
    Object? balanceControlType = freezed,
    Object? balance = null,
    Object? creditLimit = freezed,
    Object? currency = null,
    Object? extensionName = freezed,
    Object? firstname = freezed,
    Object? lastname = freezed,
    Object? email = freezed,
    Object? mobile = freezed,
    Object? companyName = freezed,
    Object? ext = freezed,
  }) {
    return _then(_$_AccountInfo(
      login: null == login
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as String,
      billingModel: null == billingModel
          ? _value.billingModel
          : billingModel // ignore: cast_nullable_to_non_nullable
              as BillingModel,
      balanceControlType: freezed == balanceControlType
          ? _value.balanceControlType
          : balanceControlType // ignore: cast_nullable_to_non_nullable
              as BalanceControlType?,
      balance: null == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as double,
      creditLimit: freezed == creditLimit
          ? _value.creditLimit
          : creditLimit // ignore: cast_nullable_to_non_nullable
              as double?,
      currency: null == currency
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      extensionName: freezed == extensionName
          ? _value.extensionName
          : extensionName // ignore: cast_nullable_to_non_nullable
              as String?,
      firstname: freezed == firstname
          ? _value.firstname
          : firstname // ignore: cast_nullable_to_non_nullable
              as String?,
      lastname: freezed == lastname
          ? _value.lastname
          : lastname // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      mobile: freezed == mobile
          ? _value.mobile
          : mobile // ignore: cast_nullable_to_non_nullable
              as String?,
      companyName: freezed == companyName
          ? _value.companyName
          : companyName // ignore: cast_nullable_to_non_nullable
              as String?,
      ext: freezed == ext
          ? _value.ext
          : ext // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_AccountInfo implements _AccountInfo {
  const _$_AccountInfo(
      {required this.login,
      required this.billingModel,
      this.balanceControlType,
      required this.balance,
      this.creditLimit,
      required this.currency,
      required this.extensionName,
      required this.firstname,
      required this.lastname,
      this.email,
      this.mobile,
      this.companyName,
      this.ext});

  factory _$_AccountInfo.fromJson(Map<String, dynamic> json) =>
      _$$_AccountInfoFromJson(json);

  @override
  final String login;
  @override
  final BillingModel billingModel;
  @override
  final BalanceControlType? balanceControlType;
  @override
  final double balance;
  @override
  final double? creditLimit;
  @override
  final String currency;
  @override
  final String? extensionName;
  @override
  final String? firstname;
  @override
  final String? lastname;
  @override
  final String? email;
  @override
  final String? mobile;
  @override
  final String? companyName;
  @override
  final String? ext;

  @override
  String toString() {
    return 'AccountInfo(login: $login, billingModel: $billingModel, balanceControlType: $balanceControlType, balance: $balance, creditLimit: $creditLimit, currency: $currency, extensionName: $extensionName, firstname: $firstname, lastname: $lastname, email: $email, mobile: $mobile, companyName: $companyName, ext: $ext)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AccountInfo &&
            (identical(other.login, login) || other.login == login) &&
            (identical(other.billingModel, billingModel) ||
                other.billingModel == billingModel) &&
            (identical(other.balanceControlType, balanceControlType) ||
                other.balanceControlType == balanceControlType) &&
            (identical(other.balance, balance) || other.balance == balance) &&
            (identical(other.creditLimit, creditLimit) ||
                other.creditLimit == creditLimit) &&
            (identical(other.currency, currency) ||
                other.currency == currency) &&
            (identical(other.extensionName, extensionName) ||
                other.extensionName == extensionName) &&
            (identical(other.firstname, firstname) ||
                other.firstname == firstname) &&
            (identical(other.lastname, lastname) ||
                other.lastname == lastname) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.mobile, mobile) || other.mobile == mobile) &&
            (identical(other.companyName, companyName) ||
                other.companyName == companyName) &&
            (identical(other.ext, ext) || other.ext == ext));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      login,
      billingModel,
      balanceControlType,
      balance,
      creditLimit,
      currency,
      extensionName,
      firstname,
      lastname,
      email,
      mobile,
      companyName,
      ext);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AccountInfoCopyWith<_$_AccountInfo> get copyWith =>
      __$$_AccountInfoCopyWithImpl<_$_AccountInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AccountInfoToJson(
      this,
    );
  }
}

abstract class _AccountInfo implements AccountInfo {
  const factory _AccountInfo(
      {required final String login,
      required final BillingModel billingModel,
      final BalanceControlType? balanceControlType,
      required final double balance,
      final double? creditLimit,
      required final String currency,
      required final String? extensionName,
      required final String? firstname,
      required final String? lastname,
      final String? email,
      final String? mobile,
      final String? companyName,
      final String? ext}) = _$_AccountInfo;

  factory _AccountInfo.fromJson(Map<String, dynamic> json) =
      _$_AccountInfo.fromJson;

  @override
  String get login;
  @override
  BillingModel get billingModel;
  @override
  BalanceControlType? get balanceControlType;
  @override
  double get balance;
  @override
  double? get creditLimit;
  @override
  String get currency;
  @override
  String? get extensionName;
  @override
  String? get firstname;
  @override
  String? get lastname;
  @override
  String? get email;
  @override
  String? get mobile;
  @override
  String? get companyName;
  @override
  String? get ext;
  @override
  @JsonKey(ignore: true)
  _$$_AccountInfoCopyWith<_$_AccountInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
