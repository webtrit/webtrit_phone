// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_contact.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AccountContact _$$_AccountContactFromJson(Map<String, dynamic> json) =>
    _$_AccountContact(
      number: json['number'] as String,
      extensionId: json['extension_id'] as String,
      extensionName: json['extension_name'] as String?,
      firstName: json['firstname'] as String?,
      lastName: json['lastname'] as String?,
      email: json['email'] as String?,
      mobile: json['mobile'] as String?,
      companyName: json['company_name'] as String?,
      sipStatus: json['sip_status'] as int,
    );

Map<String, dynamic> _$$_AccountContactToJson(_$_AccountContact instance) =>
    <String, dynamic>{
      'number': instance.number,
      'extension_id': instance.extensionId,
      'extension_name': instance.extensionName,
      'firstname': instance.firstName,
      'lastname': instance.lastName,
      'email': instance.email,
      'mobile': instance.mobile,
      'company_name': instance.companyName,
      'sip_status': instance.sipStatus,
    };
