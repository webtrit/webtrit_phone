// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppStatus _$$_AppStatusFromJson(Map<String, dynamic> json) => _$_AppStatus(
      register: json['register'] as bool,
    );

Map<String, dynamic> _$$_AppStatusToJson(_$_AppStatus instance) =>
    <String, dynamic>{
      'register': instance.register,
    };
