import 'package:flutter/material.dart';

const kApiClientConnectionTimeout = Duration(seconds: 5);

const kSignalingClientConnectionTimeout = Duration(seconds: 10);
const kSignalingClientReconnectDelay = Duration(seconds: 3);

const kCompatibilityVerifyRepeatDelay = Duration(seconds: 2);

const kInset = kMinInteractiveDimension / 2;

const kMainAppBarBottomTabHeight = 42.0;
const kMainAppBarBottomSearchHeight = kMinInteractiveDimension;
const kMainAppBarBottomPaddingGap = 6.0;

const kBlankUri = 'about:blank';
