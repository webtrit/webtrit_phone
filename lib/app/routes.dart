class AppRoute {
  AppRoute._();

  static const login = 'login';
  static const loginStep = 'login/step';

  static const webRegistration = 'web-registration';

  static const main = 'main';

  static const permissions = 'permissions';
}

class MainRoute {
  MainRoute._();

  static const call = 'call';

  static const settings = 'settings';
  static const settingsAbout = 'about';
  static const settingsHelp = 'help';
  static const settingsLanguage = 'language';
  static const settingsNetwork = 'network';
  static const settingsTermsConditions = 'terms-conditions';
  static const settingsThemeMode = 'theme-mode';
  static const logRecordsConsole = 'log-records-console';

  static const contact = 'contact';

  static const recent = 'recent';
}
