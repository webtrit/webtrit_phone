// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'login_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LoginState {
  LoginStep get step => throw _privateConstructorUsedError;
  LoginStatus get status => throw _privateConstructorUsedError;
  Object? get error => throw _privateConstructorUsedError;
  bool get demo => throw _privateConstructorUsedError;
  String? get coreUrl => throw _privateConstructorUsedError;
  SessionOtpProvisional? get otpProvisional =>
      throw _privateConstructorUsedError;
  String? get token => throw _privateConstructorUsedError;
  UrlInput get coreUrlInput => throw _privateConstructorUsedError;
  EmailInput get emailInput => throw _privateConstructorUsedError;
  PhoneInput get phoneInput => throw _privateConstructorUsedError;
  CodeInput get codeInput => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LoginStateCopyWith<LoginState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginStateCopyWith<$Res> {
  factory $LoginStateCopyWith(
          LoginState value, $Res Function(LoginState) then) =
      _$LoginStateCopyWithImpl<$Res, LoginState>;
  @useResult
  $Res call(
      {LoginStep step,
      LoginStatus status,
      Object? error,
      bool demo,
      String? coreUrl,
      SessionOtpProvisional? otpProvisional,
      String? token,
      UrlInput coreUrlInput,
      EmailInput emailInput,
      PhoneInput phoneInput,
      CodeInput codeInput});

  $SessionOtpProvisionalCopyWith<$Res>? get otpProvisional;
}

/// @nodoc
class _$LoginStateCopyWithImpl<$Res, $Val extends LoginState>
    implements $LoginStateCopyWith<$Res> {
  _$LoginStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? step = null,
    Object? status = null,
    Object? error = freezed,
    Object? demo = null,
    Object? coreUrl = freezed,
    Object? otpProvisional = freezed,
    Object? token = freezed,
    Object? coreUrlInput = null,
    Object? emailInput = null,
    Object? phoneInput = null,
    Object? codeInput = null,
  }) {
    return _then(_value.copyWith(
      step: null == step
          ? _value.step
          : step // ignore: cast_nullable_to_non_nullable
              as LoginStep,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as LoginStatus,
      error: freezed == error ? _value.error : error,
      demo: null == demo
          ? _value.demo
          : demo // ignore: cast_nullable_to_non_nullable
              as bool,
      coreUrl: freezed == coreUrl
          ? _value.coreUrl
          : coreUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      otpProvisional: freezed == otpProvisional
          ? _value.otpProvisional
          : otpProvisional // ignore: cast_nullable_to_non_nullable
              as SessionOtpProvisional?,
      token: freezed == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      coreUrlInput: null == coreUrlInput
          ? _value.coreUrlInput
          : coreUrlInput // ignore: cast_nullable_to_non_nullable
              as UrlInput,
      emailInput: null == emailInput
          ? _value.emailInput
          : emailInput // ignore: cast_nullable_to_non_nullable
              as EmailInput,
      phoneInput: null == phoneInput
          ? _value.phoneInput
          : phoneInput // ignore: cast_nullable_to_non_nullable
              as PhoneInput,
      codeInput: null == codeInput
          ? _value.codeInput
          : codeInput // ignore: cast_nullable_to_non_nullable
              as CodeInput,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $SessionOtpProvisionalCopyWith<$Res>? get otpProvisional {
    if (_value.otpProvisional == null) {
      return null;
    }

    return $SessionOtpProvisionalCopyWith<$Res>(_value.otpProvisional!,
        (value) {
      return _then(_value.copyWith(otpProvisional: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_LoginStateCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$_LoginStateCopyWith(
          _$_LoginState value, $Res Function(_$_LoginState) then) =
      __$$_LoginStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {LoginStep step,
      LoginStatus status,
      Object? error,
      bool demo,
      String? coreUrl,
      SessionOtpProvisional? otpProvisional,
      String? token,
      UrlInput coreUrlInput,
      EmailInput emailInput,
      PhoneInput phoneInput,
      CodeInput codeInput});

  @override
  $SessionOtpProvisionalCopyWith<$Res>? get otpProvisional;
}

/// @nodoc
class __$$_LoginStateCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$_LoginState>
    implements _$$_LoginStateCopyWith<$Res> {
  __$$_LoginStateCopyWithImpl(
      _$_LoginState _value, $Res Function(_$_LoginState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? step = null,
    Object? status = null,
    Object? error = freezed,
    Object? demo = null,
    Object? coreUrl = freezed,
    Object? otpProvisional = freezed,
    Object? token = freezed,
    Object? coreUrlInput = null,
    Object? emailInput = null,
    Object? phoneInput = null,
    Object? codeInput = null,
  }) {
    return _then(_$_LoginState(
      step: null == step
          ? _value.step
          : step // ignore: cast_nullable_to_non_nullable
              as LoginStep,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as LoginStatus,
      error: freezed == error ? _value.error : error,
      demo: null == demo
          ? _value.demo
          : demo // ignore: cast_nullable_to_non_nullable
              as bool,
      coreUrl: freezed == coreUrl
          ? _value.coreUrl
          : coreUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      otpProvisional: freezed == otpProvisional
          ? _value.otpProvisional
          : otpProvisional // ignore: cast_nullable_to_non_nullable
              as SessionOtpProvisional?,
      token: freezed == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      coreUrlInput: null == coreUrlInput
          ? _value.coreUrlInput
          : coreUrlInput // ignore: cast_nullable_to_non_nullable
              as UrlInput,
      emailInput: null == emailInput
          ? _value.emailInput
          : emailInput // ignore: cast_nullable_to_non_nullable
              as EmailInput,
      phoneInput: null == phoneInput
          ? _value.phoneInput
          : phoneInput // ignore: cast_nullable_to_non_nullable
              as PhoneInput,
      codeInput: null == codeInput
          ? _value.codeInput
          : codeInput // ignore: cast_nullable_to_non_nullable
              as CodeInput,
    ));
  }
}

/// @nodoc

class _$_LoginState implements _LoginState {
  const _$_LoginState(
      {required this.step,
      this.status = LoginStatus.input,
      this.error,
      this.demo = false,
      this.coreUrl,
      this.otpProvisional,
      this.token,
      this.coreUrlInput = const UrlInput.pure(),
      this.emailInput = const EmailInput.pure(),
      this.phoneInput = const PhoneInput.pure(),
      this.codeInput = const CodeInput.pure()});

  @override
  final LoginStep step;
  @override
  @JsonKey()
  final LoginStatus status;
  @override
  final Object? error;
  @override
  @JsonKey()
  final bool demo;
  @override
  final String? coreUrl;
  @override
  final SessionOtpProvisional? otpProvisional;
  @override
  final String? token;
  @override
  @JsonKey()
  final UrlInput coreUrlInput;
  @override
  @JsonKey()
  final EmailInput emailInput;
  @override
  @JsonKey()
  final PhoneInput phoneInput;
  @override
  @JsonKey()
  final CodeInput codeInput;

  @override
  String toString() {
    return 'LoginState(step: $step, status: $status, error: $error, demo: $demo, coreUrl: $coreUrl, otpProvisional: $otpProvisional, token: $token, coreUrlInput: $coreUrlInput, emailInput: $emailInput, phoneInput: $phoneInput, codeInput: $codeInput)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoginState &&
            (identical(other.step, step) || other.step == step) &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality().equals(other.error, error) &&
            (identical(other.demo, demo) || other.demo == demo) &&
            (identical(other.coreUrl, coreUrl) || other.coreUrl == coreUrl) &&
            (identical(other.otpProvisional, otpProvisional) ||
                other.otpProvisional == otpProvisional) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.coreUrlInput, coreUrlInput) ||
                other.coreUrlInput == coreUrlInput) &&
            (identical(other.emailInput, emailInput) ||
                other.emailInput == emailInput) &&
            (identical(other.phoneInput, phoneInput) ||
                other.phoneInput == phoneInput) &&
            (identical(other.codeInput, codeInput) ||
                other.codeInput == codeInput));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      step,
      status,
      const DeepCollectionEquality().hash(error),
      demo,
      coreUrl,
      otpProvisional,
      token,
      coreUrlInput,
      emailInput,
      phoneInput,
      codeInput);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LoginStateCopyWith<_$_LoginState> get copyWith =>
      __$$_LoginStateCopyWithImpl<_$_LoginState>(this, _$identity);
}

abstract class _LoginState implements LoginState {
  const factory _LoginState(
      {required final LoginStep step,
      final LoginStatus status,
      final Object? error,
      final bool demo,
      final String? coreUrl,
      final SessionOtpProvisional? otpProvisional,
      final String? token,
      final UrlInput coreUrlInput,
      final EmailInput emailInput,
      final PhoneInput phoneInput,
      final CodeInput codeInput}) = _$_LoginState;

  @override
  LoginStep get step;
  @override
  LoginStatus get status;
  @override
  Object? get error;
  @override
  bool get demo;
  @override
  String? get coreUrl;
  @override
  SessionOtpProvisional? get otpProvisional;
  @override
  String? get token;
  @override
  UrlInput get coreUrlInput;
  @override
  EmailInput get emailInput;
  @override
  PhoneInput get phoneInput;
  @override
  CodeInput get codeInput;
  @override
  @JsonKey(ignore: true)
  _$$_LoginStateCopyWith<_$_LoginState> get copyWith =>
      throw _privateConstructorUsedError;
}
